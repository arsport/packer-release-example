role = ENV['INSPEC_ROLE']
curl_release_version = ENV['CURL_VERSION']

#Verify that we have the Techops repos package
describe package('curl') do
  it { should be_installed }
  its('version') { should eq curl_release_version }
end

#Verify that we have the cloud-init is installed
describe package('inspec') do
  it { should be_installed }
end

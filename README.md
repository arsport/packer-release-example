# Packer release example
Hashicorp Packer (https://packer.io/) is a wonderful tool to create O.S. images out of code.

https://www.slideshare.net/AlejandroRosa7/image-release-process-for-security-wins-228744274

### What is so great about that?

- Faster application deployment
- Consistent application environment
- Remove unwanted dependencies
- Remove the unnecessary weight
- Treat your images as code
- But most important thing…. Secure yourself.
### Good practices around building your own images
- Clean up after building and old stuff
- Don't put secrets into your images
- Restrict the account to build (not use your personal account)
## The why of this example
I noticed how good examples of using packer specially in a release process were poor to null.
I handled a project in my team which required several teams building secure AMI's with a lot of goodies in a controlled environment, which generated problem along the way. That did not mean I was unmotivated but it took me a good amount to time to get into a very good project that will satisfy all the requirements that I wanted.
### Requirements
My opinionated requirements I guess...
- Selfserved
- Easy access to the latest images
- CI/CD type
- Based on templates
- Differentiate your test and prod images
- Automated testing
- Added security (Least access, audits, restrict sudo, ACL's, remove unnecessary packages and patching installed applications, configuration management)